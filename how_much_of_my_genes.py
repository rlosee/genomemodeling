import os
os.system("cls")
import random

class Generation_Chain():
    """A Generation Chain"""

    def __init__(self, parent_name):
        """Instantiate Generation_Chain"""
        # a generation chain has "links" of generations
        self.chain = Person(0, parent_name)
        #return super().__init__(*args, **kwargs)

    def make_link(self, link):
        """Make Generation Chain Link"""
        self.link = Person()

class Person():
    """A Person Class"""

    def __init__(self, gen_num, parent_name, parent_genome):
        """Initialize a person."""
        if gen_num == 0:
            # initialize the zero-th generation
            self.name = parent_name # this is generation 0
            self.genome = Genome(parent_name)

        else:
            # make next generation
            self.name = parent_name + 'G'
            self.genome = Genome(parent_name, parent_genome)

class Genome():
    """Make a Person's Genome"""
    karyotype = 3  # number of chromosome pairs

    def __init__(self, parent_name, parent_genome=[]):
        if not parent_genome:
            self.chromosomes = []
            for i in range(1, Genome.karyotype+1):
                # a list of tuples
                self.chromosomes.append((parent_name + '-' + str(i) + 'A', parent_name + '-' + str(i) + 'B'))
            return None

        # OK, it's not the 0 generation so combine parents' genomes   
        self.chromosomes = [] 
        for (a,b) in parent_genome.chromosomes:
            self.chromosomes.append( (random.choice( (a, b) ), '--na--'))

me = Person(0)
print(me.genome.chromosomes)

child = Person(1, me.name, me.genome)
print(child.genome.chromosomes)

gen_limit = 12
#me = Person(1,'Bob',[(1,2)])


