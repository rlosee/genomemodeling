import os
os.system("cls")

import random

#import csv
#with open ('how_man_of_my_chromosomes.csv', mode='w') as descendants_file:
#    descendants_writer = csv.writer(descendants_file, delimiter=',', )

f=open('how_many_of_my_chromosomes.csv', "w", encoding="utf-8")

class Descendants():
    """ Make a Line of Descendants"""

    def __init__(self, parent_name, parent_gen_num, parent_genome):
        """Instantiate A Line of Descendants"""

        if parent_gen_num + 1 <= gen_limit:
            self.next_gen = Person(parent_name, parent_gen_num, parent_genome)
            self.my_descendants = Descendants(self.next_gen.name, self.next_gen.gen_num, self.next_gen.genome)

class Person():
    """A Person Class"""

    def __init__(self, parent_name, parent_gen_num=None, parent_genome=None):
        """Initialize a person."""
        if parent_gen_num is None:
            # initialize the zero-th generation
            self.name = parent_name # this is generation 0
            self.gen_num = 0
            self.genome = Genome(parent_name)

        else:
            # make next generation
            self.gen_num = parent_gen_num + 1
            if self.gen_num == 1:
                self.name = parent_name + 'C'
            elif self.gen_num == 2:
                self.name = parent_name + 'g'
            else:
                self.name = parent_name + 'G'

            self.genome = Genome(self.name, self.gen_num, parent_genome)

class Genome():
    """Make a Person's Genome"""
    karyotype = 23  # number of chromosome pairs

    def __init__(self, this_name, this_gen=0, parent_genome=[]):
        if not parent_genome:
            # generation 0 (me)
            self.chromosomes = [] 
            for i in range(1, Genome.karyotype+1):
                # a list of tuples
                self.chromosomes.append((this_name + '-' + str(i) + 'A', this_name + '-' + str(i) + 'B'))
        else:
            # OK, it's not the 0 generation so combine parents' genomes   
            self.chromosomes = [] 
            for (a,b) in parent_genome.chromosomes:
                pair_choice = random.choice((a, b))
                if pair_choice != 'Ø':
                    self.chromosomes.append((pair_choice, 'Ø'))

            # The following count pairs, but after me (gen 0) only one of the pairs can
            # have my genetics. So divide the pairs by 2.
            how_much = (len(self.chromosomes)/23)/2 
            print('\n\n' + this_name, "{:.2%}".format(how_much), self.chromosomes)

            f.write(str(this_gen) + "," + this_name + "," + "{:.4f}".format(how_much) + "\n")
            #descendants_writer.writerow([this_gen, this_name, how_much])


gen_limit = 12

me = Person("M", None, None)

for i in range(0, 1000):
    my_descendants = Descendants(me.name, me.gen_num, me.genome)
#print(me.chain.name)
#my_chain.make_links()

f.close()
print("ALL DONE")

#me = Person(0, "Ǽ", "")
#print('\n', me.genome.chromosomes)
#
#child = Person(1, me.name, me.genome)
#print('\n', child.genome.chromosomes)

##me = Person(1,'Bob',[(1,2)])


